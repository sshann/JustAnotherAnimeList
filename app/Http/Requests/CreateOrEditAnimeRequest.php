<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrEditAnimeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $animeID = $this->route('anime.edit');
        return [
            'title'             => 'string|required|unique:animes,id,'.$animeID.'|max:255',
            'episode-count'     => 'present|nullable|integer|min:1',
            'episode-duration'  => 'present|nullable|integer|min:1',
            'start-date'        => 'present|nullable|date|required_with:finish_date',
            'finish-date'       => 'present|nullable|date|after:start_date',
            'description'       => 'present|nullable|max:65535|string',
        ];
    }
}
