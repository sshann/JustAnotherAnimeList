<?php

namespace App\Http\Controllers;

use App\Anime;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use App;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\CreateOrEditAnimeRequest;

class AnimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $animes = DB::table('animes')
            ->orderBy('title')
            ->paginate(20);
        
        return view('anime.anime-index')
            ->with('animes',$animes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('anime.anime-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\CreateOrEditAnimeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOrEditAnimeRequest $request)
    {
        $anime = new Anime();
        $anime->title = $request->input('title');
        $anime->episode_count = $request->input('episode-count');
        $anime->episode_duration = $request->input('episode-duration');
        $anime->start_date = $request->input('start-date');
        $anime->finish_date = $request->input('finish-date');
        $anime->description = nl2br($request->input('description'));
        $anime->save();

        return redirect('animes');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Anime  $anime
     * @return \Illuminate\Http\Response
     */
    public function show(Anime $anime)
    {
        return view('anime.anime-show')
            ->with('anime',$anime);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Anime  $anime
     * @return \Illuminate\Http\Response
     */
    public function edit(Anime $anime)
    {
        return view('anime.anime-edit')
            ->with('anime',$anime);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\CreateOrEditAnimeRequest  $request
     * @param  \App\Anime  $anime
     * @return \Illuminate\Http\Response
     */
    public function update(CreateOrEditAnimeRequest $request, Anime $anime)
    {
        $anime->title = $request->input('title');
        $anime->episode_count = $request->input('episode-count');
        $anime->episode_duration = $request->input('episode-duration');
        $anime->start_date = $request->input('start-date');
        $anime->finish_date = $request->input('finish-date');
        $anime->description = nl2br($request->input('description'));
        $anime->save();

        return redirect('animes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Anime  $anime
     * @return \Illuminate\Http\Response
     */
    public function destroy(Anime $anime)
    {
        //
    }
}
