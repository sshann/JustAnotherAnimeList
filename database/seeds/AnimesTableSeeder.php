<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AnimesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('animes')->insert([
            'title' => 'Shuumatsu Nani Shitemasu ka? Isogashii desu ka? Sukutte Moratte Ii desu ka?',
            'episode_count' => 24,
            'start_date' => Carbon::create('2017', '04', '11'),
            'description' => 'Five hundred years have passed since the humans went extinct at the hands of the fearsome and mysterious \'Beasts.\' The surviving races now make their homes up on floating islands in the sky, out of reach of all but the most mobile of Beasts.\nOnly a small group of young girls, the Leprechauns, can wield the ancient weapons needed to fend off invasions from these creatures. Into the girls\' unstable and fleeting lives, where a call to certain death could come at any moment, enters an unlikely character: a young man who lost everything in his final battle five hundred years ago, the last living human awakened from a long, icy slumber.\nUnable to fight any longer, Willem becomes the father that the girls never had, caring for and nurturing them even as he struggles to come to terms with his new life, in which he feels the pain of helplessly waiting for his loved ones to return home from battle that his \'Daughter\' once felt for him so long ago. Together, Willem and the girls gradually come to understand what family means and what is truly worth protecting.',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
		DB::table('animes')->insert([
        	'title' => 'Nyanko Days',
            'episode_duration' => 2,
            'episode_count' => 12,
            'start_date' => Carbon::create('2017', '01', '08'),
            'finish_date' => Carbon::create('2017', '03', '26'),
            'description' => 'Konagai Tomoko is a first-year in high school and a shy girl. Tomoko owns three cats. The cheerful and live Munchkin Maa, the smart and responsible Russian Blue Rou, and the gentle crybaby Singapura Shii. Tomoko, whose only friends were her cats, one day becomes friends with Shiratori Azumi, who also loves cats. This is a fluffy and cute comedy about the daily life of Tomoko and her cats, Tomoko and Azumi\'s friendship, and the interaction between cats.',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
		DB::table('animes')->insert([
        	'title' => 'Gosick',
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
