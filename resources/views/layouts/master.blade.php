<html>
	<head>
		<title>Just Another Anime List | @yield('title', 'Home Page')</title>
	</head>
	<body>
		@include('partials.sidebar')
		<div class="container">
		@yield('content')
		</div>
		@section('footerScripts')
			<script src="app.js"></script>
		@show
	</body>
</html>