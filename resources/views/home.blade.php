@extends('layouts.master')

@section('title', 'Dashboard')

@section('content')
    
    Welcome to your application dashboard!
    <div class="content" data-page-name="Dashboard">
        <p>Here's why you should sign up for our app: <strong>It's Great.</strong></p>
        @include('sign-up-button', ['text' => 'See just how great it is', 'pageName' => 'Dashboard'])
        <br>
        <a href="<?php echo route('animes.index'); ?>"> Anime </a>
    </div>
@endsection

@section('footerScripts')
    @parent
    <script src="dashboard.js"></script>
@endsection

