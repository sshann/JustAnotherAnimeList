@extends('layouts.master')

@section('title', 'Anime show')

@section('content')
    <h2> {{ $anime->title }} </h2>
	
	<a href="/animes"> @lang('navigation.back')</a>
    @include('partials.errors')
	<ul>
		<li> <b> @lang('anime.episode_count'): </b> {{ $anime->episode_count or trans('anime.unknown') }}
		<li> <b> @lang('anime.episode_duration'): </b> {{ $anime->episode_duration or trans('anime.unknown') }}
			@if ($anime->episode_duration)
				 minutes					
			@endif 
		<li> <b> @lang('anime.aired_period'): </b> {{ $anime->start_date or trans('anime.unknown') }} to {{ $anime->finish_date or trans('anime.unknown') }}
		<li> <b> @lang('anime.description'): </b> {{ $anime->description or trans('anime.unknown') }}

	</ul>
@endsection

@section('footerScripts')
    @parent
@endsection

