@extends('layouts.master')

@section('title', 'Anime index')

@section('content')
    <h2> @lang('anime.anime_list') </h2>
	
	<a href="/">@lang('navigation.back')</a>
	<br>
    @include('partials.errors')
    <br>
    <a href="{{ route('animes.create') }}">@lang('anime.create_new_anime')</a>
    <ul>
		@forelse ($animes as $anime)
			<li> 
				<b>{{ $loop->iteration }}.</b> <a href="{{ URL('animes/'.$anime->id) }}">{{ $anime->title }}</a>
				<ul>
					<li> 
						<a href="{{ route('animes.edit', [$anime->id]) }}">@lang('anime.edit_anime')</a>
					</li>
				</ul>
			</li>
		@empty
			No anime stored yet.
		@endforelse
	</ul>

	{{ $animes->links() }}
@endsection

@section('footerScripts')
    @parent
@endsection

