{{ csrf_field() }}
@lang('anime.title'):<input value="{{ $anime->title or old('title') }}" type="text" name="title" placeholder=@lang('anime.title')>
<br>
@lang('anime.episode_count'): <input value="{{ $anime->episode_count or old('episode-count') }}" type="integer" name="episode-count" placeholder=@lang('anime.episode_count')> 
<br>
@lang('anime.episode_duration'): <input value="{{ $anime->episode_duration or old('episode-duration') }}" type="integer" name="episode-duration" placeholder=@lang('anime.episode_duration')> 
<br>
@lang('anime.start_date'): <input value="{{ $anime->start_date or old('start-date') }}" type="date" name="start-date" placeholder=@lang('anime.start_date')> 
<br>
@lang('anime.finish_date'): <input value="{{ $anime->finish_date or old('finish-date') }}" type="date" name="finish-date" placeholder=@lang('anime.finish_date')> 
<br>
@lang('anime.description'):<textarea name="description" form="form-anime" placeholder=@lang('anime.description')>{{ $anime->description or old('description') }}</textarea>
<br>
<input type="submit">
