@extends('layouts.master')

@section('title', 'Anime index')

@section('content')
    <h2> @lang('anime.edit_anime') </h2>
	
	<a href="/animes">@lang('navigation.back')</a>
	<div>
		@include('partials.errors')
		<form id="form-anime" method="POST" enctype="multipart/form-data" action="/animes/{{$anime->id}}">
			<input type="hidden" name="_method" value="PATCH">
	    	@include('anime.anime-form')
		</form>
	</div>
@endsection

@section('footerScripts')
    @parent
@endsection