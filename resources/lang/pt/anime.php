<?php

return [
	'anime_list' => 'Lista de Animes',
	'episode_count' => 'Quantidade de episódios',
	'episode_duration' => 'Duração dos episódios',
	'aired_period' => 'Exibição',
	'description' => 'Descrição',
	'unknown' => 'Desconhecido',
	'create_new_anime' => 'Cadastrar um novo anime',
	'start_date' => 'Data de inicio',
	'finish_date' => 'Data de fim',
	'title' => 'Título',
	'edit_anime' => 'Editar anime',
];