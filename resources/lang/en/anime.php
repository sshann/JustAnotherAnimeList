<?php

return [
	'anime_list' => 'Anime List',
	'episode_count' => 'Episodes count',
	'episode_duration' => 'Episodes duration',
	'aired_period' => 'Aired',
	'description' => 'Description',
	'unknown' => 'Unknown',
	'create_new_anime' => 'Insert new anime',
	'start_date' => 'Start date',
	'finish_date' => 'Finish date',
	'title' => 'Title',
	'edit_anime' => 'Edit anime',
];